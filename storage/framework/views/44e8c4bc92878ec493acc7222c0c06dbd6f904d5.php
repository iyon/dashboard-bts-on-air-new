<!DOCTYPE html>
<html lang="en" dir="ltr">
    <head>
        <meta charset="utf-8">
        <title></title>
    </head>
    <body>
        <div class="row">
            <div class="card-body" style="height:330px; padding-top: 3px; ">
                <div class="chart" style="height:330px; background-color: #343a40;">
                    <canvas id="barChart2"></canvas>
                </div>
            </div>
        </div>
    </body>

    <script src="<?php echo e(url('')); ?>/js/jquery.min.js"></script>

    <script type="text/javascript">
    $(document).ready(function(){
        $.ajax({
            url:'http://10.54.36.49/bts-rekon/api/get_openclose_total',
            type:'get',
            success: function(result){
              var regions = []
              var opens = []
              var closes = []
              var total = []
              $.each(JSON.parse(result), function(idx, obj) {
                   regions.push(obj.REGIONAL);
                   opens.push(obj.OPEN);
                   closes.push(obj.CLOSE);
                   total.push(obj.TOTAL);
              });
              console.log(regions);
                var areaChartData = {
                  labels  : ['R1','R2','R3','R4','R5','R6','R7','R8','R9','R10','R11'],
                  datasets: [
                    {
                      label               : 'Open',
                      fillColor           : '#fff',
                      strokeColor         : '#c2c7d0',
                      pointColor          : '#fff',
                      pointStrokeColor    : '#fff',
                      pointHighlightFill  : '#fff',
                      pointHighlightStroke: '#fff',
                      data                : opens
                    },
                    {
                      label               : 'Close',
                      fillColor           : '#008080',
                      strokeColor         : '#c2c7d0',
                      pointColor          : '#008080',
                      pointStrokeColor    : '#008080',
                      pointHighlightFill  : '#008080',
                      pointHighlightStroke: '#008080',
                      data                : closes
                    }
                  ]
                }
                //-------------
                //- BAR CHART -
                //-------------
                var barChartCanvas                   = $('#barChart2').get(0).getContext('2d')
                var barChart                         = new Chart(barChartCanvas)
                var barChartData                     = areaChartData
                barChartData.datasets[1].fillColor   = '#008080'
                barChartData.datasets[1].strokeColor = '##c2c7d0'
                barChartData.datasets[1].pointColor  = '#008080'
                var barChartOptions                  = {
                  //Boolean - Whether the scale should start at zero, or an order of magnitude down from the lowest value
                  scaleBeginAtZero        : true,
                  //Boolean - Whether grid lines are shown across the chart
                  scaleShowGridLines      : true,
                  //String - Colour of the grid lines
                  scaleGridLineColor      : '#0000',
                  scaleFontColor          :'#ffffff',
                  scaleLineColor          : '#ffffff',
                  //Number - Width of the grid lines
                  scaleGridLineWidth      : 1,
                  //Boolean - Whether to show horizontal lines (except X axis)
                  scaleShowHorizontalLines: true,
                  //Boolean - Whether to show vertical lines (except Y axis)
                  scaleShowVerticalLines  : true,
                  //Boolean - If there is a stroke on each bar
                  barShowStroke           : true,
                  //Number - Pixel width of the bar stroke
                  barStrokeWidth          : 2,
                  //Number - Spacing between each of the X value sets
                  barValueSpacing         : 5,
                  //Number - Spacing between data sets within X values
                  barDatasetSpacing       : 1,
                  //String - A legend template
                  legendTemplate          : '<ul class="<%=name.toLowerCase()%>-legend"><% for (var i=0; i<datasets.length; i++){%><li><span style="background-color:<%=datasets[i].fillColor%>"></span><%if(datasets[i].label){%><%=datasets[i].label%><%}%></li><%}%></ul>',
                  //Boolean - whether to make the chart responsive
                  responsive              : true,
                  maintainAspectRatio     : true
                }

                barChartOptions.datasetFill = false
                barChart.Bar(barChartData, barChartOptions)
            }
                    });
    });
    </script>
</html>
