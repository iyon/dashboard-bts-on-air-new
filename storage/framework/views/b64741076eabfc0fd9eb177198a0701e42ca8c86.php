<?php header('Access-Control-Allow-Origin: *'); ?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>NEISA | DASHBOARD</title>
    <!-- Tell the browser to be responsive to screen width -->

    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Font Awesome -->
    
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="<?php echo e(url('')); ?>/dist/css/adminlte.min.css">
    <!-- iCheck -->
    <link rel="stylesheet" href="<?php echo e(url('')); ?>/plugins/iCheck/flat/blue.css">
    <!-- Morris chart -->
    <link rel="stylesheet" href="<?php echo e(url('')); ?>/plugins/morris/morris.css">
    <!-- bootstrap wysihtml5 - text editor -->
    <!-- DataTables -->
    <link rel="stylesheet" href="<?php echo e(url('')); ?>/plugins2/datatables/dataTables.bootstrap4.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="<?php echo e(url('')); ?>/dist2/css/adminlte.min.css">
    <link rel="stylesheet" href="<?php echo e(url('')); ?>/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">

     <!-- *** START TAMBAH IMPORT FONTAWESOME IYON *** -->
    <!-- <link rel="stylesheet" type="text/css" href="./asset/plugins/fontawesome-free-5.6.3-web/css/fontawesome.css">
    <link rel="stylesheet" type="text/css" href="./asset/plugins/fontawesome-free-5.6.3-web/css/solid.css">
    <link rel="stylesheet" type="text/css" href="./asset/plugins/fontawesome-free-5.6.3-web/css/brands.css"> -->
    <link rel="stylesheet" type="text/css" href="<?php echo e(url('')); ?>/plugins/fontawesome-free-5.6.3-web/css/fontawesome.css">
    <link rel="stylesheet" type="text/css" href="<?php echo e(url('')); ?>/plugins/fontawesome-free-5.6.3-web/css/solid.css">
    <link rel="stylesheet" type="text/css" href="<?php echo e(url('')); ?>/plugins/fontawesome-free-5.6.3-web/css/brands.css">
    <!-- *** END TAMBAH IMPORT FONTAWESOME IYON *** -->

    <style>
        /* ** START TAMBAH CSS IYON** */
        .dropClass {
          /* display: none; */
            visibility: hidden;
            opacity: 0;
            transition: 
            all .5s ease;
            background-color: transparent;
            min-width: 160px;
            overflow: auto;
            z-index: 1;
            height: 0;
            /* margin-bottom: -5%; */
          
        }
        
        .dropClass a {
          color: black;
          padding: 12px 16px;
          /* text-decoration: none; */
          display: block;
        }
        
        .dropClass a:hover {background-color: rgba(255,255,255,.1);}
        .show {
            /* display: block; */
            visibility: visible;
            opacity: 1;
            height: auto;
            padding: 0.5rem 1rem;
            background-color: rgba(255,255,255,.3);
            margin-bottom: 1.5%;
            border-radius: 3%;
        }
    
        .putar {
            transform: rotate(90deg);
            transition: all .5s ease;
        }

.brand-image {
line-height: .8;
max-height: 53px;
width: auto;
margin-left: 0.7rem;
margin-right: .5rem;
margin-top: -3px;
float: none;
opacity: .9;
}

.backgroundImg {
width: auto;
height: 100%;
opacity: 1;
position: absolute;
}

.backgroundImg2 {
position: fixed;
width: 100%;
max-height: 56px;
margin-left: -2%;
opacity: 1;
}

.nav-item:hover {
background-color: rgba(255,255,255,.3);
border-radius: 5%;
transition: all .2s ease;
}

.active {
background-color: rgba(243, 255, 226, .8) !important;
color: #343a40 !important;
font-weight: 600;
}

.berisik {
min-height:500px !important
}
/* ** END TAMBAH IYON** */
    </style>

</head>
<body class="hold-transition sidebar-mini" style="background: #f4f6f9; color: white;">
    <div class="wrapper">

        <!-- Navbar -->
         <nav class="main-header navbar navbar-expand bg-dark" style="margin-left: 250px; position:fixed; width:100%;">
            <img src="<?php echo e(url('')); ?>/dist/img/wall5.jpg" class="backgroundImg2" style="position: fixed;
    width: 100%;">
   
            <!-- Left navbar links -->
      <ul class="navbar-nav" style="z-index: 999;">
        <li class="nav-item">
          <a class="nav-link" data-widget="pushmenu" href="#"><i class="fa fa-bars nav-button" style="color:white;"></i></a>
        </li>
        <li class=" navbar-brand" style="color:white; margin-left: 10%;">NEISA | DASHBOARD</li>
        <li class="nav-item">
          <a class="nav-link btn btn-lg" href="http://10.54.36.49/landingPage/" onclick="sessionStorage.clear();" style="
                  color: #343a40 !important;
                  background-color: #fff;
                  position: absolute;
                  font-size: 10px;
                  right: 6%;
                  height: auto;
                  box-shadow: 0 2px 5px 0 rgba(0,0,0,.16), 0 2px 10px 0 rgba(0,0,0,.12);
                  text-transform: uppercase;
                  font-family: Roboto;
                  padding: 1%;"><i class="fa fa-sign-out-alt"></i> Log Out</a>
        </li>
      </ul>
    </nav>

        <!-- Main Sidebar Container -->
        <aside class="main-sidebar sidebar-dark-primary elevation-4 berisik">
            <img src="<?php echo e(url('')); ?>/dist/img/wall3.jpg" class="backgroundImg">
     
            <!-- Brand Logo -->
            <a href="#" class="brand-link">
        <img src="<?php echo e(url('')); ?>/dist/img/tsel-white.png"
             style="opacity: .8; float:none; widht:200px; line-height:.8; max-height:53px;margin-left:0.7rem;margin-right:.5rem;margin-top:-3px">
      </a>

            <!-- Sidebar -->
            <div class="sidebar">
                <!-- Sidebar user panel (optional) -->
                <!-- Sidebar Menu -->
                <nav class="mt-2">
                        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                                <li class="nav-item">
                                    <a href="http://10.54.36.49/dashboard-bts-on-air/public/" class="nav-link aa active" style="color: #fff;padding: 0.5rem 1rem !important;">
                                    <i class="nav-icon fa fa-home"></i>
                                        <p style="margin-left: 3px;">Dashboard</p>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a href="http://10.54.36.49/dashboard-license" class="nav-link aa " style="color: #fff;padding: 0.5rem 1rem !important;">
                                    <i class="nav-icon fa fa-newspaper"></i>
                                        <p style="margin-left: 3px;">License</p>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a href="http://10.54.36.49/btsonair" class="nav-link " style="color: #fff;padding: 0.5rem 1rem !important;">
                                    <i class="nav-icon fa fa-broadcast-tower"></i>
                                        <p style="margin-left: 3px;">BTS On Air</p>
                                    </a>
                                </li>
                
                                <li class="nav-item" style="cursor: pointer;">
                                    <a onclick="dropDead()" class="nav-link aa dropbtn" style="color: #fff;padding: 0.5rem 1rem !important;">
                                        <i class="nav-icon fa fa-angle-right" id="dropIcon"></i>
                                        <p style="margin-left: 3px;">Create</p>
                                    </a>
                                </li>
                
                                
                                <div class="dropClass" id="dropId">
                                <li class="nav-item">
                                    <a href="http://10.54.36.49/apk-nodin/index.php/NodinController" class="nav-link aa" style="color: #fff;padding: 0.5rem 1rem !important;">
                                        <i class="nav-icon fa fa-list-alt"></i>
                                        <p style="margin-left: 3px;">Create Integrasi</p>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a href="http://10.54.36.49/apk-nodin-stylo/index.php/NodinController" class="nav-link aa" style="color: #fff;padding: 0.5rem 1rem !important;">
                                        <i class="nav-icon fa fa-list-alt"></i>
                                        <p style="margin-left: 3px;">Create Rehoming</p>
                                    </a>
                                </li>
                                <li class="nav-item">
                                  <a href="http://10.54.36.49/apk-nodin-dismantle/index.php/NodinController" class="nav-link aa " style="color: #fff;padding: 0.5rem 1rem !important;">
                                    <i class="nav-icon fa fa-list-alt"></i>
                                      <p style="margin-left: 3px;">Create Dismantle</p>
                                  </a>
                              </li>
                              <li class="nav-item">
                                  <a href="http://10.54.36.49/apk-nodin-relocation/index.php/NodinController" class="nav-link aa " style="color: #fff;padding: 0.5rem 1rem !important;">
                                    <i class="nav-icon fa fa-list-alt"></i>
                                      <p style="margin-left: 3px;">Create Relocation</p>
                                  </a>
                              </li>
                              <li class="nav-item">
                                <a href="http://10.54.36.49/apk-nodin-swap/index.php/NodinController" class="nav-link aa" style="color: #fff;padding: 0.5rem 1rem !important;">
                                    <i class="nav-icon fa fa-list-alt"></i>
                                    <p style="margin-left: 3px;">Create Swap</p>
                                </a>
                            </li>
                        </div>
                                <li class="nav-item">
                                    <a href="http://10.54.36.49/change-front-2/public/" class="nav-link aa " style="color: #fff;padding: 0.5rem 1rem !important;">
                                    <i class="nav-icon fa fa-project-diagram"></i>
                                        <p style="margin-left: 3px;">Project Tracking</p>
                                    </a>
                                </li>
                                <li class="nav-item">
                                  <a href="http://10.54.36.49/tableList" class="nav-link aa" style="color: #fff;padding: 0.5rem 1rem !important;">
                                  <i class="nav-icon fa fa-table"></i>
                                      <p style="margin-left: 3px;">Table List</p>
                                  </a>
                              </li>
                              <li class="nav-item">
                                <a href="http://10.54.36.49/apk-report/index.php/ReportController" class="nav-link aa " style="color: #fff;padding: 0.5rem 1rem !important;">
                                    <i class="nav-icon fa fa-book"></i>
                                    <p style="margin-left: 3px;">Report Remedy</p>
                                </a>
                            </li>
                          </ul>
                    </nav>
                    <!-- /.sidebar-menu -->
                </div>
                <!-- /.sidebar -->
            </aside>

            <!-- Content Wrapper. Contains page content -->
            <div class="content-wrapper" style="margin-top:60px; background-color: grey; "  >
                
                <!-- <nav aria-label="breadcrumb"  >
                    <ol class="breadcrumb" style="background-image: linear-gradient(-90deg, #434343 , #000000);">
                        <li class="breadcrumb-item active" aria-current="page" >BTS On Air</li>
                        <li class="breadcrumb-item"><a href="<?php echo e(url('index2')); ?>">Data Consistency</a></li>
                    </ol>
                </nav>
           -->
                <section class="content">
                    <div class="container-fluid" style="background-color: black;">
                        <section class="col-lg-12">
                            <div class="row">
                                <div class="col-12">
                                    <div class="card" style="margin-top:15px;">
                                      <!--  <div class="card-header" style="background-color: #008080;">
                                            <h3 class="card-title" style="color: white;">Data Nasional</h3>
                                        </div> -->
                                        <div class="card-body" style="background-color: black;">
                                            <!-- Small boxes (Stat box) -->
                                            <div class="col-sm-12 mt-3">
                                                <div class="row">
                                                    <div class="col-lg-3 col-md-2" >
                                                        <div class="small-box bg-warning" style="background-image: linear-gradient(-90deg, #96deda , #50c9c3); box-shadow: 0 4px 8px 0 white; ">
                                                            <div class="inner">
                                                                <div class="col-xs-12 text-right" style="font-color:30px;">
                                                                    <div class="pull-left" class="huge" style="font-size:30px; text-shadow: 3px 2px 1px solid black bold;">2G</div>
                                                                    <!--<div class="huge" style="font-size:40px; text-shadow: 3px 2px 1px solid black;" id="total_on_2g">0</div> -->
                                                                    <div class="huge" style="font-size:40px; text-shadow: 3px 2px 1px solid black;" >50,310</div>
                                                                    <div style="font-size:14px; text-shadow: 3px 2px 1px solid black;"><strong>Total On Air</strong></div>
                                                                </div>
                                                                <br/>
                                                                
                                                                <div class="clearfix"></div>
                                                            </div>
                                                                 <div class="container" style="background-color: white;">
                                                       <!-- <span style="font-size:16px; float: left; width: auto;">New &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;0</span></span>
                                                                 | 
                                                                
                                                                <span style="font-size:16px; float: right; ">Dismantle&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;0</span></span> -->
                                                        <span style="font-size:16px; float: left; width: auto;">New &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;0</span></span>
| 
                                                                
                                                                <span style="font-size:16px; ">Dismantle&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;0</span></span>       
							 <div class="clearfix"></div>
                                                        </div>
                                                        </div>
                                                    </div>
                                                    <!-- ./col -->
                                                    <div class="col-lg-3 col-md-2" >
                                                        <div class="small-box bg-warning" style="background-image: linear-gradient(-90deg, #a1c4fd , #c2e9fb); box-shadow: 0 4px 8px 0 white;">
                                                            <div class="inner">
                                                                <div class="col-xs-12 text-right">
                                                                    <div class="pull-left" class="huge" style="font-size:30px; text-shadow: 3px 2px 1px solid black;">3G</div>
                                                                    <!--<div class="huge" style="font-size:40px; text-shadow: 3px 2px 1px solid black;" id="total_on_3g">0</div>-->
                                                                    <div class="huge" style="font-size:40px; text-shadow: 3px 2px 1px solid black;" >82,118</div>
                                                                    <div style="font-size:14px; text-shadow: 3px 2px 1px solid black;"><strong>Total On Air</strong></div>
                                                                </div>
                                                               <br/>
                                                                
                                                                <div class="clearfix"></div>
                                                            </div>
                                                                 <div class="container" style="background-color: white;">
                                                        <span style="font-size:16px;">New &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;0</span></span>
                                                                 | 
                                                                
                                                                <span style="font-size:16px; ">Dismantle&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;1</span></span>
                                                                <div class="clearfix"></div>
                                                        </div>
                                                        </div>
                                                    </div>
                                                    <!-- ./col -->
                                                    <div class="col-lg-3 col-md-2">
                                                        <div class="small-box bg-warning" style="background-image: linear-gradient(-90deg, #ffecd2 , #fcb69f); box-shadow: 0 4px 8px 0 white;">
                                                            <div class="inner">
                                                                <div class="col-xs-12 text-right">
                                                                    <div class="pull-left" class="huge" style="font-size:30px; text-shadow: 3px 2px 1px solid black;">4G</div>
                                                                    <!--<div class="huge" style="font-size:40px; text-shadow: 3px 2px 1px solid black;" id="total_on_4g">0</div> -->
                                                                    <div class="huge" style="font-size:40px; text-shadow: 3px 2px 1px solid black;" >56,653</div>
                                                                    <div style="font-size:14px; text-shadow: 3px 2px 1px solid black;"><strong>Total On Air</strong></div>
                                                                </div>
                                                                <br/>
                                                               
                                                                <div class="clearfix"></div>
                                                            </div>
                                                                 <div class="container" style="background-color: white;">
                                                        <span style="font-size:16px;">New &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;568</span></span>
                                                                 | 
                                                                
                                                                <span style="font-size:16px; ">Dismantle&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;1</span></span>
                                                                <div class="clearfix"></div>
                                                        </div>
                                                        </div>
                                                    </div>
                                                    <!-- ./col -->
                                                    <div class="col-lg-3 col-md-2">
                                                        <div class="small-box bg-warning" style="background-image: linear-gradient(-90deg, #96deda , #50c9c3); box-shadow: 0 4px 8px 0 white;">
                                                            <div class="inner">
                                                                <div class="col-xs-12 text-right">
                                                                    <div class="pull-left" class="huge" style="font-size:30px; text-shadow: 3px 2px 1px solid black;">Total</div>
                                                                    <!--<div class="huge" style="font-size:40px; text-shadow: 3px 2px 1px solid black;" id="total_on_total">0</div> -->
                                                                    <div class="huge" style="font-size:40px; text-shadow: 3px 2px 1px solid black;" >189,081</div>
                                                                    <div style="font-size:14px; text-shadow: 3px 2px 1px solid black;"><strong>Total On Air</strong></div>
                                                                </div>
                                                                <br/>
                                                               
                                                                <div class="clearfix"></div>
                                                            </div>
                                                                 <div class="container" style="background-color: white;">
                                                        <span style="font-size:16px;">New &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;0</span></span>
                                                                 | 
                                                                
                                                                <span style="font-size:16px; ">Dismantle&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;0</span></span>
                                                                <div class="clearfix"></div>
                                                        </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </section>

                                <section class="col-lg-12">
                                    <div class="row">
                                        <div class="col-6" >

                                           <!-- <div class="card"> -->
                                               <!-- <div class="card-header" style="background-color: #008080;">
                                                    <h3 class="card-title" style="color: white;">Data Regional</h3>
                                                </div> -->
                                                <!-- /.card-header -->
                                                <!--<div class="card-body" style="background-color: #343a40;"> -->
                                                    <table id="example1" class="table table-bordered table-hover" style="table-layout: fixed; border-collapse: collapse; background-color: #343a40; font-size:14px; padding: 0%;">
                                                        <thead style="background-image: linear-gradient(-90deg, #80d0c7 , #13547a); ">
                                                            <tr style="color:white;">
                                                                <th rowspan="2" style="text-align:center;vertical-align:middle">REGIONAL</th>
                                                                <th colspan="2" style="text-align:center">2G</th>
                                                                <th colspan="2" style="text-align:center">3G</th>
                                                                <th style="text-align:center">4G</th>
                                                            </tr>
                                                            <tr style="color:white;">
                                                                <th>BSC</th>
                                                                <th>BTS</th>
                                                                <th>RNC</th>
                                                                <th>NODEB</th>
                                                                <th>ENODEB</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody id="resume_sysinfo" style="color:white;">

                                                        </tbody>
                                                    </table>
                                                <!--</div> -->
                                                <!-- /.card-body -->
                                            <!---</div> -->
                                            <!-- /.card -->
                                        </div>
                                        <div class="col-lg-6">
                                            <div class="card">
                                                <div class="card-header" style="background-image: linear-gradient(-90deg, #80d0c7 , #13547a ); ">
                                                    <h3 style="color: white;" class="card-title"><i class="fa fa-th mr-1"></i>Count of BTS On Air (Region)</h3>
                                                    <div class="card-tools">
                                                        <button type="button" class="btn btn-tool" data-widget="collapse">
                                                            <i class="fa fa-minus"></i>
                                                        </button>
                                                        <button type="button" class="btn btn-tool" data-widget="remove">
                                                            <i class="fa fa-times"></i>
                                                        </button>
                                                    </div>
                                                </div>
                                                <div class="card-body" style="background-color: #343a40;">
                                                    <?php echo $__env->make('dashboardbts3', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- /.col -->
                                    </div>
                                    <!-- /.row -->
                                </section>
                                <section class="col-lg-12 col-lg-6">
                                    <div class="row">
                                        <div class="col-lg-6">
                                            <div class="card">
                                                <div class="card-header" style="background-image: linear-gradient(-90deg, #80d0c7 , #13547a ); ">
                                                    <h3 style="color: white;" class="card-title">
                                                        <i class="fa fa-th mr-1"></i>
                                                        BTS Need To Rekon
                                                    </h3>
                                                    <div class="card-tools">
                                                        <button type="button" class="btn btn-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                                                        </button>
                                                        <button type="button" class="btn btn-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                                                    </div>
                                                </div>
                                                <div class="card-body" style="background-color: #343a40;">
                                                    <center>
                                                        <label class="pull-center" style="color:white;">Rekon &nbsp;</label><button style="width: 12px; height: 12px; background-color: #fff; border: 0px"></button>&nbsp;&nbsp;&nbsp;
                                                        <label class="pull-center" style="color:white;">Non Rekon &nbsp;</label><button style="width: 12px; height: 12px; background-color: #008080; border: 0px;"></button>
                                                    </center>
                                                    <?php echo $__env->make('dashboardbts', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                                                </div>
                                                <!-- /.card-body -->
                                            </div>
                                        </div>
                                        <div class="col-lg-6">
                                            <div class="card">
                                                <div class="card-header" style="background-image: linear-gradient(-90deg, #80d0c7 , #13547a ); ">
                                                    <h3 style="color: white;" class="card-title">
                                                        <i class="fa fa-th mr-1"></i>
                                                        Ticket Open and Close
                                                    </h3>
                                                    <div class="card-tools">
                                                        <button type="button" class="btn btn-tool" data-widget="collapse">
                                                            <i class="fa fa-minus"></i>
                                                        </button>
                                                        <button type="button" class="btn btn-tool" data-widget="remove">
                                                            <i class="fa fa-times"></i>
                                                        </button>
                                                    </div>
                                                </div>
                                                <div class="card-body" style="background-color: #343a40;">
                                                    <center>
                                                        <label class="pull-center" style="color:white;">Open &nbsp;</label><button style="width: 12px; height: 12px; background-color: #fff; border: 0px"></button>&nbsp;&nbsp;&nbsp;
                                                        <label class="pull-center" style="color:white;">Close &nbsp;</label><button style="width: 12px; height: 12px;  background-color: #008080; border: 0px;"></button>
                                                    </center>
                                                    <?php echo $__env->make('dashboardbts2', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </section>
                                <!--<section class="col-lg-12 col-lg-6">
                                    <div class="row">
                                        <div class="col-lg-6">
                                            <div class="card">
                                                <div class="card-header" style="background-image: linear-gradient(-90deg, #80d0c7 , #13547a ); ">
                                                    <h3 style="color: white;" class="card-title"><i class="fa fa-th mr-1"></i>Count of BTS On Air (Region)</h3>
                                                    <div class="card-tools">
                                                        <button type="button" class="btn btn-tool" data-widget="collapse">
                                                            <i class="fa fa-minus"></i>
                                                        </button>
                                                        <button type="button" class="btn btn-tool" data-widget="remove">
                                                            <i class="fa fa-times"></i>
                                                        </button>
                                                    </div>
                                                </div>
                                                <div class="card-body" style="background-color: #343a40;">
                                                    <?php echo $__env->make('dashboardbts3', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-6">
                                            <div class="card">
                                                <div class="card-header" style="background-image: linear-gradient(-90deg, #80d0c7 , #13547a ); ">
                                                    <h3 style="color: white;" class="card-title"><i class="fa fa-th mr-1"></i>Count Of BTS Dismantled</h3>
                                                    <div class="card-tools">
                                                        <button type="button" class="btn btn-tool" data-widget="collapse">
                                                            <i class="fa fa-minus"></i>
                                                        </button>
                                                        <button type="button" class="btn btn-tool" data-widget="remove">
                                                            <i class="fa fa-times"></i>
                                                        </button>
                                                    </div>
                                                </div>
                                                <div class="card-body" style="background-color: #343a40;">
                                                    <?php echo $__env->make('dashboardbts4', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </section> -->
                                <section class="col-lg-12 col-lg-6">
                                    <div class="row">
                                        <div class="col-lg-6">
                                            <div class="card">
                                                <div class="card-header" style="background-image: linear-gradient(-90deg, #80d0c7 , #13547a ); ">
                                                    <h3 style="color: white;" class="card-title"><i class="fa fa-th mr-1"></i>Count of BTS On Air New (Region)</h3>
                                                    <div class="card-tools">
                                                        <button type="button" class="btn btn-tool" data-widget="collapse">
                                                            <i class="fa fa-minus"></i>
                                                        </button>
                                                        <button type="button" class="btn btn-tool" data-widget="remove">
                                                            <i class="fa fa-times"></i>
                                                        </button>
                                                    </div>
                                                </div>
                                                <div class="card-body" style="background-color: #343a40;">
                                                    <?php echo $__env->make('dashboardbts5', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-6">
                                            <div class="card">
                                                <div class="card-header" style="background-image: linear-gradient(-90deg, #80d0c7 , #13547a ); ">
                                                    <h3 style="color: white;" class="card-title"><i class="fa fa-th mr-1"></i>Count of BTS On Air Existing (Region)</h3>
                                                    <div class="card-tools">
                                                        <button type="button" class="btn btn-tool" data-widget="collapse">
                                                            <i class="fa fa-minus"></i>
                                                        </button>
                                                        <button type="button" class="btn btn-tool" data-widget="remove">
                                                            <i class="fa fa-times"></i>
                                                        </button>
                                                    </div>
                                                </div>
                                                <div class="card-body" style="background-color: #343a40;">
                                                    <?php echo $__env->make('dashboardbts6', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </section>
                        </div>
                    </div>

                    <footer class="main-footer">
                        <strong style="font-size: 12px">Copyright &copy; 2018 <a href="https://www.telkomsel.com">Telkomsel</a>.</strong>
                    </footer>

                    <!-- Control Sidebar -->
                    <aside class="control-sidebar control-sidebar-dark">
                        <!-- Control sidebar content goes here -->
                    </aside>
                    <!-- /.control-sidebar -->
                </div>

                <script src="<?php echo e(url('')); ?>/plugins2/jquery/jquery.min.js"></script>
                <script src="<?php echo e(url('')); ?>/plugins2/datatables/jquery.dataTables.js"></script>
                <script src="<?php echo e(url('')); ?>/plugins2/datatables/dataTables.bootstrap4.js"></script>
                <script src="<?php echo e(url('')); ?>/plugins2/slimScroll/jquery.slimscroll.min.js"></script>
                <script src="<?php echo e(url('')); ?>/plugins2/fastclick/fastclick.js"></script>
                <script src="<?php echo e(url('')); ?>/dist2/js/adminlte.min.js"></script>
                <script src="<?php echo e(url('')); ?>/dist2/js/demo.js"></script>

                <script type="text/javascript">
                function logout(){
                    sessionStorage.remove('token', '');
                    window.location.href = "<?php echo e(env('APP_URL')); ?>/landingPage/";
                }
            </script>
            <script src="<?php echo e(url('')); ?>/plugins/jquery/jquery.min.js"></script>
            <!-- jQuery UI 1.11.4 -->
            <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.min.js"></script>
            <!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
            <script src="<?php echo e(url('')); ?>/js/jquery-ui.min.js"></script>
            <script>
            $.widget.bridge('uibutton', $.ui.button)
        </script>
        <script>

function dropDead() {
    document.getElementById("dropIcon").classList.toggle('putar');
    document.getElementById("dropId").classList.toggle("show");
  }
        $("#nav-open").click(function(){
            if (document.getElementById("wrapper").style.padding == "0px") {
                $("#wrapper").css('padding-left', '150px');
                $("#sidebar-wrapper").css('width', '150px');
            }else {
                document.getElementById("wrapper").style.padding = "0";
                document.getElementById("sidebar-wrapper").style.width = "0";
            }
        });
        function formatNumber(num) {
                    return num.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,')
                    }

        $(document).ready(function(){
            $.ajax({
                url:"http://10.54.36.49/bts-rekon/api/get_summary_all",
                type:"get",
                success: function(result){
                    var hasil = JSON.parse(result);
                    
                    $('#total_on_2g').html(hasil.total_on_2g.toLocaleString('en'));
                    $('#new_2g').html(hasil.new_2g);
                    $('#existing_2g').html(hasil.existing_2g);
                    $('#dismantle_2g').html(hasil.dismantle_2g);
                    $('#total_on_3g').html(hasil.total_on_3g.toLocaleString('en'));
                    $('#new_3g').html(hasil.new_3g);
                    $('#existing_3g').html(hasil.existing_3g);
                    $('#dismantle_3g').html(hasil.dismantle_3g);
                    $('#total_on_4g').html(hasil.total_on_4g.toLocaleString('en'));
                    $('#new_4g').html(hasil.new_4g);
                    $('#existing_4g').html(hasil.existing_4g);
                    $('#dismantle_4g').html(hasil.dismantle_4g);
                    $('#total_on_total').html(hasil.total_on_total.toLocaleString('en'));
                    $('#new_total').html(hasil.new_total);
                    $('#existing_total').html(hasil.existing_total);
                    $('#dismantle_total').html(hasil.dismantle_total);
                    console.log(hasil);
                }
            });
            $.ajax({
                url:"http://10.54.36.49/api-btsonair/public/api/bts_summary_2",
                type:"get",
                success: function(result){
                    $('#resume_sysinfo').empty();
                    for (var i = 0; i < result.length; i++) {
                        console.log(result[i].regional);
                        $('#resume_sysinfo').append('<tr><td>'+result[i].regional+'</td><td>'+result[i].data1+'</td><td>'+result[i].data2+'</td><td>'+result[i].data3+'</td><td>'+result[i].data4+'</td><td>'+result[i].data5+'</td></tr>');
                    }
                }
            });
            //2g
            // $.ajax({
            //     url:'http://localhost/apis-bts-rekon/api/total_on_2g',
            //     type:'get',
            //     success: function(result){
            //         $('#total_on_2g').html(result);
            //
            //     }
            // });
            // $.ajax({
            //     url:'http://localhost/apis-bts-rekon/api/new_2g',
            //     type:'get',
            //     success: function(result){
            //         $('#new_2g').html('New Site&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i class="fa" style="color:green">&#xf062;&nbsp;</i>'+result);
            //     }
            // });
            //     $.ajax({
            //      url:'http://localhost/apis-bts-rekon/api/existing_2g',
            //     type:'get',
            //     success: function(result){
            //       console.log(result);
            //         $('#existing_2g').html('Existing&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i class="fa" style="color:green">&#xf062;&nbsp;</i>'+result);
            //     }
            // });
            // $.ajax({
            //      url:'http://localhost/apis-bts-rekon/api/dismantle_2g',
            //     type:'get',
            //     success: function(result){
            //         $('#dismantle_2g').html('Dismantle&nbsp;&nbsp;<i class="fa" style="color:green">&#xf062;&nbsp;</i>'+result);
            //     }
            // });
            //
            // //3g
            //  $.ajax({
            //     url:'http://localhost/apis-bts-rekon/api/total_on_3g',
            //     type:'get',
            //     success: function(result){
            //         $('#total_on_3g').html(result);
            //
            //     }
            // });
            // $.ajax({
            //     url:'http://localhost/apis-bts-rekon/api/new_3g',
            //     type:'get',
            //     success: function(result){
            //         $('#new_3g').html('New Site&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i class="fa" style="color:green">&#xf062;&nbsp;</i>'+result);
            //     }
            // });
            //     $.ajax({
            //      url:'http://localhost/apis-bts-rekon/api/existing_3g',
            //     type:'get',
            //     success: function(result){
            //       console.log(result);
            //         $('#existing_3g').html('Existing&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i class="fa" style="color:green">&#xf062;&nbsp;</i>'+result);
            //     }
            // });
            // $.ajax({
            //      url:'http://localhost/apis-bts-rekon/api/dismantle_3g',
            //     type:'get',
            //     success: function(result){
            //         $('#dismantle_3g').html('Dismantle&nbsp;&nbsp;<i class="fa" style="color:green">&#xf062;&nbsp;</i>'+result);
            //     }
            // });
            //
            // //4g
            //  $.ajax({
            //     url:'http://localhost/apis-bts-rekon/api/total_on_4g',
            //     type:'get',
            //     success: function(result){
            //         $('#total_on_4g').html(result);
            //
            //     }
            // });
            // $.ajax({
            //     url:'http://localhost/apis-bts-rekon/api/new_4g',
            //     type:'get',
            //     success: function(result){
            //         $('#new_4g').html('New Site&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i class="fa" style="color:green">&#xf062;&nbsp;</i>'+result);
            //     }
            // });
            //     $.ajax({
            //      url:'http://localhost/apis-bts-rekon/api/existing_4g',
            //     type:'get',
            //     success: function(result){
            //       console.log(result);
            //         $('#existing_4g').html('Existing&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i class="fa" style="color:green">&#xf062;&nbsp;</i>'+result);
            //     }
            // });
            // $.ajax({
            //      url:'http://localhost/apis-bts-rekon/api/dismantle_4g',
            //     type:'get',
            //     success: function(result){
            //         $('#dismantle_4g').html('Dismantle&nbsp;&nbsp;<i class="fa" style="color:green">&#xf062;&nbsp;</i>'+result);
            //     }
            // });
            //
            // //total
            //  $.ajax({
            //     url:'http://localhost/apis-bts-rekon/api/total_on_total',
            //     type:'get',
            //     success: function(result){
            //         $('#total_on_total').html(result);
            //
            //     }
            // });
            // $.ajax({
            //     url:'http://localhost/apis-bts-rekon/api/new_total',
            //     type:'get',
            //     success: function(result){
            //         $('#new_total').html('New Site&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i class="fa" style="color:green">&#xf062;&nbsp;</i>'+result);
            //     }
            // });
            //     $.ajax({
            //      url:'http://localhost/apis-bts-rekon/api/existing_total',
            //     type:'get',
            //     success: function(result){
            //       console.log(result);
            //         $('#existing_total').html('Existing&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i class="fa" style="color:green">&#xf062;&nbsp;</i>'+result);
            //     }
            // });
            // $.ajax({
            //      url:'http://localhost/apis-bts-rekon/api/dismantle_total',
            //     type:'get',
            //     success: function(result){
            //         $('#dismantle_total').html('Dismantle&nbsp;&nbsp;<i class="fa" style="color:green">&#xf062;&nbsp;</i>'+result);
            //     }
            // });
        });
    </script>
    <script src="<?php echo e(url('')); ?>/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
    <!-- Morris.js charts -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>
    <script src="<?php echo e(url('')); ?>/plugins/morris/morris.min.js"></script>
    <!-- Sparkline -->
    <script src="<?php echo e(url('')); ?>/plugins/sparkline/jquery.sparkline.min.js"></script>
    <!-- jvectormap -->
    <script src="<?php echo e(url('')); ?>/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
    <script src="<?php echo e(url('')); ?>/plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
    <!-- jQuery Knob Chart -->
    <script src="<?php echo e(url('')); ?>/plugins/knob/jquery.knob.js"></script>
    <!-- daterangepicker -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.2/moment.min.js"></script>
    <script src="<?php echo e(url('')); ?>/plugins/daterangepicker/daterangepicker.js"></script>
    <!-- datepicker -->
    <script src="<?php echo e(url('')); ?>/plugins/datepicker/bootstrap-datepicker.js"></script>
    <!-- Bootstrap WYSIHTML5 -->
    <script src="<?php echo e(url('')); ?>/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>
    <!-- Slimscroll -->
    <script src="<?php echo e(url('')); ?>/plugins/slimScroll/jquery.slimscroll.min.js"></script>
    <!-- FastClick -->
    <script src="<?php echo e(url('')); ?>/plugins/fastclick/fastclick.js"></script>
    <!-- AdminLTE App -->
    <script src="<?php echo e(url('')); ?>/dist/js/adminlte.js"></script>
    <!-- AdminLTE dashboard demo (This is only for demo purposes) -->
    <script src="<?php echo e(url('')); ?>/dist/js/pages/dashboard.js"></script>
    <!-- AdminLTE for demo purposes -->
    <script src="<?php echo e(url('')); ?>/dist/js/demo.js"></script>
    <script src="<?php echo e(url('')); ?>/plugins/chartjs-old/Chart.min.js"></script>
</body>
</html>
